//---------------------------------------------------------------------------
// Color.h
// Header file defining Color in SGL (Simple Graphics Library)
// Date:  2021/10/18
// Author: Zdenek Kolar
//---------------------------------------------------------------------------
#ifndef _SGL_COLOR_H_
#define _SGL_COLOR_H_

/// struct for representing sgl RGB color format 
struct sglColor {
    float R, G, B;
    sglColor(float r = 0, float g = 0, float b = 0) : R(r), G(g), B(b) {}
    inline void setColor(float r, float g, float b) { R = r; G = g; B = b; }
};

#endif

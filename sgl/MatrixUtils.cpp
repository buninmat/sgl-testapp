//---------------------------------------------------------------------------
// matrixUtils.cpp
// Implementation file for matrix utility functions in SGL (Simple Graphics Library)
// Date:  2021/10/11
// Author: Zdenek Kolar
//---------------------------------------------------------------------------
#include "MatrixUtils.h"

void sglMatrixUtils::mult(const float* left, const float* right, float* res) {
    static float tmp[16];

    // compute by columns
    for (int i = 0; i < 16; i += 4) {
        tmp[i] = left[0] * right[i] + left[4] * right[i + 1] + left[8] * right[i + 2] + left[12] * right[i + 3];
        tmp[i+1] = left[1] * right[i] + left[5] * right[i + 1] + left[9] * right[i + 2] + left[13] * right[i + 3];
        tmp[i+2] = left[2] * right[i] + left[6] * right[i + 1] + left[10] * right[i + 2] + left[14] * right[i + 3];
        tmp[i+3] = left[3] * right[i] + left[7] * right[i + 1] + left[11] * right[i + 2] + left[15] * right[i + 3];

    }
    res[0] = tmp[0];    res[1] = tmp[1];    res[2] = tmp[2];    res[3] = tmp[3];
    res[4] = tmp[4];    res[5] = tmp[5];    res[6] = tmp[6];    res[7] = tmp[7];
    res[8] = tmp[8];    res[9] = tmp[9];    res[10] = tmp[10];  res[11] = tmp[11];
    res[12] = tmp[12];  res[13] = tmp[13];  res[14] = tmp[14];  res[15] = tmp[15];
}

sglVertex sglMatrixUtils::multVertex(const float* mat, const sglVertex& vert) {
    static sglVertex ret;
    ret.x = mat[0] * vert.x + mat[4] * vert.y + mat[8] * vert.z + mat[12] * vert.w;
    ret.y = mat[1] * vert.x + mat[5] * vert.y + mat[9] * vert.z + mat[13] * vert.w;
    ret.z = mat[2] * vert.x + mat[6] * vert.y + mat[10] * vert.z + mat[14] * vert.w;
    ret.w = mat[3] * vert.x + mat[7] * vert.y + mat[11] * vert.z + mat[15] * vert.w;
    return ret;
}

sglVertex sglMatrixUtils::toScreenSpace(const float* PVM, const float* viewport, const sglVertex& worldSPoint) {
    sglVertex ret = multVertex(PVM, worldSPoint);
    if (ret.w != 1) {
        float w_div = 1 / ret.w;
        ret.x *= w_div;
        ret.y *= w_div;
        ret.z *= w_div;
        ret.w = 1;
    }
    return multVertex(viewport, ret);
}


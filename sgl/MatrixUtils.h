//---------------------------------------------------------------------------
// matrixUtils.h
// Header file defining matrix utility functions in SGL (Simple Graphics Library)
// Date:  2021/10/11
// Author: Zdenek Kolar
//---------------------------------------------------------------------------
#ifndef _SGL_MATRIX_UTILS_H_
#define _SGL_MATRIX_UTILS_H_

#include "sgl.h"
#include "Vertex.h"

class sglMatrixUtils
{
public:
	/**
	* Multiplies two matrices
	* 
	* @param left [in] matrix on the left side of an operand
	* @param right [in] matrix on the right side of an operand
	* @param ret [in] PREALLOCATED matrix to write the result to
	*/
	static void mult(const float* left, const float* right, float* res);
	/**
	* Multiplies a point by matrix
	* 
	* @param mat [in] matrix on the left side of an operand
	* @param vert [in] vertex to be multiplied
	* 
	* @return Multiplied vertex
	*/
	static sglVertex multVertex(const float* mat, const sglVertex& vertex);

	/**
	* Transform a given point in world space to screen space
	* 
	* @param PVM [in] projection-view-model matrix
	* @param viewport [in] viewport matrix
	* @param worldSPoint [in] point to transform
	* 
	* @return point in screen space coordinates
	*/
	static sglVertex toScreenSpace(const float* PVM, const float* viewport, const sglVertex& worldSPoint);
};

#endif


//---------------------------------------------------------------------------
// renderer.cpp
// Implementation file defining rendering algorithms for sglEElementType in SGL (Simple Graphics Library)
// Date:  2021/10/11
// Author: Zdenek Kolar
//---------------------------------------------------------------------------
#include <iostream>
#include "Renderer.h"
#include "Vertex.h"


/// Context to render to
static sglContext* currContext;

void sglRenderer::DrawElementsInContext(sglContext* context) {
	if (context == nullptr || context->vertices.size() <= 0) {
		return;
	}
	currContext = context;
	currContext->recalculatePVM();
	float* PVM = currContext->getPVM();
	float* viewport = currContext->getViewport();

	switch (context->currentElementType) {
		// draw points
		case SGL_POINTS: {
			sglVertex screenP;
			while (currContext->vertices.size() > 0) {
				screenP = sglMatrixUtils::toScreenSpace(PVM, viewport, currContext->vertices.front());
				currContext->vertices.pop();
				DrawPoint(screenP);
			}
			break;
		}
		// draw lines
		case SGL_LINES: {
			sglVertex screenP1, screenP2;
			while (currContext->vertices.size() > 1) {
				screenP1 = sglMatrixUtils::toScreenSpace(PVM, viewport, currContext->vertices.front());
				currContext->vertices.pop();
				screenP2 = sglMatrixUtils::toScreenSpace(PVM, viewport, currContext->vertices.front());
				currContext->vertices.pop();
				DrawLine(screenP1, screenP2);
			}
			break;
		}

		// line strip
		case SGL_LINE_STRIP: {
			sglVertex screenP1 = sglMatrixUtils::toScreenSpace(PVM, viewport, currContext->vertices.front());
			currContext->vertices.pop();
			while (currContext->vertices.size() > 0) {
				sglVertex screenP2 = sglMatrixUtils::toScreenSpace(PVM, viewport, currContext->vertices.front());
				currContext->vertices.pop();
				DrawLine(screenP1, screenP2);
				screenP1 = screenP2;
			}
			break;
		}

		// line loop
		case SGL_LINE_LOOP: {
			sglVertex start, screenP2;
			sglVertex screenP1 = sglMatrixUtils::toScreenSpace(PVM, viewport, currContext->vertices.front());
			start = screenP1;
			currContext->vertices.pop();
			while (currContext->vertices.size() > 0) {
				screenP2 = sglMatrixUtils::toScreenSpace(PVM, viewport, currContext->vertices.front());
				currContext->vertices.pop();
				DrawLine(screenP1, screenP2);
				screenP1 = screenP2;
			}
			DrawLine(screenP2, start);
			break;
		}
	default:
		while (currContext->vertices.size()) {
			currContext->vertices.pop();
		}
		break;
	}

	currContext = nullptr;
}

void sglRenderer::DrawLine(const sglVertex& screenP1, const sglVertex& screenP2) {
	int startX = (int)screenP1.x, startY = (int)screenP1.y;
	int endX = (int)screenP2.x, endY = (int)screenP2.y;

	// choose longer axis
	bool longerX = std::abs(endX - startX) >= std::abs(endY - startY);
	int shorterAxisDelta = longerX ? endY - startY : endX - startX;

	// shorter axis always increasing
	if (shorterAxisDelta < 0) {
		startX = (int)screenP2.x; startY = (int)screenP2.y;
		endX = (int)screenP1.x; endY = (int)screenP1.y;
		shorterAxisDelta *= -1;
	}

	// compute coefs for brasenham like the line is increasing in the first quadrant of a screen
	int longerAxisDelta = longerX ? endX - startX : endY - startY;

	// increments in 1D array for each axis
	int longerAxisInc = longerX ? 1 : currContext->width;
	longerAxisInc *= longerAxisDelta < 0 ? -1 : 1;
	int shorterAxisInc = longerX ? (currContext->width + longerAxisInc) :
		(1 + longerAxisInc);

	// assign pixel by index to avoid idx derivation
	int idx = startX + startY * currContext->width;
	int endIdx = endX + endY * currContext->width;

	// simple lines
	if (shorterAxisDelta == 0 || longerAxisDelta == shorterAxisDelta) {
		if (longerAxisDelta == shorterAxisDelta) {
			longerAxisInc = shorterAxisInc;
		}
		while (idx != endIdx) {
			idx += longerAxisInc;
			currContext->setPixelColorAt(idx);
		}
		return;
	}

	int absLongerDelta = std::abs(longerAxisDelta);
	int c0 = 2 * shorterAxisDelta;
	int c1 = c0 - 2 * absLongerDelta;
	int p = c0 - absLongerDelta;
	
	// for longer lines try to split to equal pieces
	if (absLongerDelta >= 24) {
		int idxInc;
		// eight same pieces
		if (absLongerDelta % 8 == 0 && shorterAxisDelta % 8 == 0) {
			idxInc = ((absLongerDelta - shorterAxisDelta) >> 3) * longerAxisInc;
			idxInc += (shorterAxisDelta >> 3) * shorterAxisInc;
			int idx2 = idx + idxInc;
			int idx3 = idx2 + idxInc;
			int idx4 = idx3 + idxInc;
			int idx5 = idx4 + idxInc;
			int idx6 = idx5 + idxInc;
			int idx7 = idx6 + idxInc;
			int idx8 = idx7 + idxInc;

			currContext->setPixelColorAt(idx8);
			while (idx8 != endIdx) {
				currContext->setPixelColorAt(idx);
				currContext->setPixelColorAt(idx2);
				currContext->setPixelColorAt(idx3);
				currContext->setPixelColorAt(idx4);
				currContext->setPixelColorAt(idx5);
				currContext->setPixelColorAt(idx6);
				currContext->setPixelColorAt(idx7);
				if (p < 0) {
					p += c0;
					idx += longerAxisInc;
					idx2 += longerAxisInc;
					idx3 += longerAxisInc;
					idx4 += longerAxisInc;
					idx5 += longerAxisInc;
					idx6 += longerAxisInc;
					idx7 += longerAxisInc;
					idx8 += longerAxisInc;

				}
				else {
					p += c1;
					idx += shorterAxisInc;
					idx2 += shorterAxisInc;
					idx3 += shorterAxisInc;
					idx4 += shorterAxisInc;
					idx5 += shorterAxisInc;
					idx6 += shorterAxisInc;
					idx7 += shorterAxisInc;
					idx8 += shorterAxisInc;
				}
				currContext->setPixelColorAt(idx8);
			}
			return;
		}
		// seven pieces
		else if (absLongerDelta % 7 == 0 && shorterAxisDelta % 7 == 0) {
			idxInc = ((absLongerDelta - shorterAxisDelta) / 7) * longerAxisInc;
			idxInc += (shorterAxisDelta / 7) * shorterAxisInc;
			int idx2 = idx + idxInc;
			int idx3 = idx2 + idxInc;
			int idx4 = idx3 + idxInc;
			int idx5 = idx4 + idxInc;
			int idx6 = idx5 + idxInc;
			int idx7 = idx6 + idxInc;
			currContext->setPixelColorAt(idx7);
			while (idx7 != endIdx) {
				currContext->setPixelColorAt(idx);
				currContext->setPixelColorAt(idx2);
				currContext->setPixelColorAt(idx3);
				currContext->setPixelColorAt(idx4);
				currContext->setPixelColorAt(idx5);
				currContext->setPixelColorAt(idx6);
				if (p < 0) {
					p += c0;
					idx += longerAxisInc;
					idx2 += longerAxisInc;
					idx3 += longerAxisInc;
					idx4 += longerAxisInc;
					idx5 += longerAxisInc;
					idx6 += longerAxisInc;
					idx7 += longerAxisInc;
				}
				else {
					p += c1;
					idx += shorterAxisInc;
					idx2 += shorterAxisInc;
					idx3 += shorterAxisInc;
					idx4 += shorterAxisInc;
					idx5 += shorterAxisInc;
					idx6 += shorterAxisInc;
					idx7 += shorterAxisInc;
				}
				currContext->setPixelColorAt(idx7);
			}
			return;
		}
		// six pieces
		else if (absLongerDelta % 6 == 0 && shorterAxisDelta % 6 == 0) {
			idxInc = ((absLongerDelta - shorterAxisDelta) / 6) * longerAxisInc;
			idxInc += (shorterAxisDelta / 6) * shorterAxisInc;
			int idx2 = idx + idxInc;
			int idx3 = idx2 + idxInc;
			int idx4 = idx3 + idxInc;
			int idx5 = idx4 + idxInc;
			int idx6 = idx5 + idxInc;

			currContext->setPixelColorAt(idx6);
			while (idx6 != endIdx) {
				currContext->setPixelColorAt(idx);
				currContext->setPixelColorAt(idx2);
				currContext->setPixelColorAt(idx3);
				currContext->setPixelColorAt(idx4);
				currContext->setPixelColorAt(idx5);
				if (p < 0) {
					p += c0;
					idx += longerAxisInc;
					idx2 += longerAxisInc;
					idx3 += longerAxisInc;
					idx4 += longerAxisInc;
					idx5 += longerAxisInc;
					idx6 += longerAxisInc;
				}
				else {
					p += c1;
					idx += shorterAxisInc;
					idx2 += shorterAxisInc;
					idx3 += shorterAxisInc;
					idx4 += shorterAxisInc;
					idx5 += shorterAxisInc;
					idx6 += shorterAxisInc;

				}
				currContext->setPixelColorAt(idx6);
			}
			return;
		}
		// five pieces
		else if (absLongerDelta % 5 == 0 && shorterAxisDelta % 5 == 0) {
			idxInc = ((absLongerDelta - shorterAxisDelta) / 5) * longerAxisInc;
			idxInc += (shorterAxisDelta / 5) * shorterAxisInc;
			int idx2 = idx + idxInc;
			int idx3 = idx2 + idxInc;
			int idx4 = idx3 + idxInc;
			int idx5 = idx4 + idxInc;

			currContext->setPixelColorAt(idx5);
			while (idx5 != endIdx) {
				currContext->setPixelColorAt(idx);
				currContext->setPixelColorAt(idx2);
				currContext->setPixelColorAt(idx3);
				currContext->setPixelColorAt(idx4);
				if (p < 0) {
					p += c0;
					idx5 += longerAxisInc;
					idx4 += longerAxisInc;
					idx3 += longerAxisInc;
					idx2 += longerAxisInc;
					idx += longerAxisInc;
				}
				else {
					p += c1;
					idx5 += shorterAxisInc;
					idx4 += shorterAxisInc;
					idx3 += shorterAxisInc;
					idx2 += shorterAxisInc;
					idx += shorterAxisInc;
				}
				currContext->setPixelColorAt(idx5);
			}
			return;
		}
		// four pieces
		else if (absLongerDelta % 4 == 0 && shorterAxisDelta % 4 == 0) {
			idxInc = ((absLongerDelta - shorterAxisDelta) >> 2) * longerAxisInc;
			idxInc += (shorterAxisDelta >> 2)* shorterAxisInc;
			int idx2 = idx + idxInc;
			int idx3 = idx2 + idxInc;
			int idx4 = idx3 + idxInc;

			currContext->setPixelColorAt(idx4);
			while (idx4 != endIdx) {
				currContext->setPixelColorAt(idx);
				currContext->setPixelColorAt(idx2);
				currContext->setPixelColorAt(idx3);
				if (p < 0) {
					p += c0;
					idx4 += longerAxisInc;
					idx3 += longerAxisInc;
					idx2 += longerAxisInc;
					idx += longerAxisInc;
				}
				else {
					p += c1;
					idx4 += shorterAxisInc;
					idx3 += shorterAxisInc;
					idx2 += shorterAxisInc;
					idx += shorterAxisInc;
				}
				currContext->setPixelColorAt(idx4);

			}
			return;
		}
		// three pieces
		else if (absLongerDelta % 3 == 0 && shorterAxisDelta % 3 == 0) {
			idxInc = ((absLongerDelta - shorterAxisDelta) /3) * longerAxisInc;
			idxInc += (shorterAxisDelta / 3) * shorterAxisInc;
			int idx2 = idx + idxInc;
			int idx3 = idx2 + idxInc;

			currContext->setPixelColorAt(idx3);
			while (idx3 != endIdx) {
				currContext->setPixelColorAt(idx);
				currContext->setPixelColorAt(idx2);
				if (p < 0) {
					p += c0;
					idx3 += longerAxisInc;
					idx2 += longerAxisInc;
					idx += longerAxisInc;
				}
				else {
					p += c1;
					idx3 += shorterAxisInc;
					idx2 += shorterAxisInc;
					idx += shorterAxisInc;
				}
				currContext->setPixelColorAt(idx3);
			}
			return;
		}
		// two pieces
		else if (absLongerDelta % 2 == 0 && shorterAxisDelta % 2 == 0) {
			int idxInc = ((absLongerDelta - shorterAxisDelta) >> 1) * longerAxisInc;
			idxInc += (shorterAxisDelta >> 1) * shorterAxisInc;
			int idx2 = idx + idxInc;
			currContext->setPixelColorAt(idx2);
			while (idx2 != endIdx) {
				currContext->setPixelColorAt(idx);
				if (p < 0) {
					p += c0;
					idx2 += longerAxisInc;
					idx += longerAxisInc;
				}
				else {
					p += c1;
					idx2 += shorterAxisInc;
					idx += shorterAxisInc;
				}
				currContext->setPixelColorAt(idx2);

			}
			return;
		}
	}
	// simple brasenham
	currContext->setPixelColorAt(idx);
	while (idx != endIdx) {
		if (p < 0) {
			p += c0;
			idx += longerAxisInc;
		}
		else {
			p += c1;
			idx += shorterAxisInc;
		}
		currContext->setPixelColorAt(idx);
	}
}

void sglRenderer::DrawPoint(const sglVertex& screenP) {
	int x = (int)(screenP.x - currContext->pointSize * 0.5f);
	int y = (int)(screenP.y - currContext->pointSize * 0.5f);
	int fromIdx = x + y * currContext->width;
	int sizeInt = (int)currContext->pointSize;
	int diff = currContext->width - sizeInt;
	int line = 0;
	
	while (line < sizeInt) {
		for (int j = 0; j < sizeInt; j++) {
			currContext->setPixelColorAt(fromIdx++);
		}
		fromIdx += diff;
		line++;
	}
}
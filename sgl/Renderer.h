//---------------------------------------------------------------------------
// renderer.h
// Header file defining rendering algorithms for sglEElementType in SGL (Simple Graphics Library)
// Date:  2021/10/11
// Author: Zdenek Kolar
//---------------------------------------------------------------------------
#ifndef _SGL_RENDERING_H_
#define _SGL_RENDERING_H_

#include "context.h"

struct sglRenderer
{
	/**
	* Draws a line between two points using Brasenham algorithm
	* 
	* @param screenP1 [in] the starting point of the line in screen space coordinate system
	* @param screenP2 [in] the ending point of the line in screen space coordinate system
	*/
	static void DrawLine(const sglVertex& screenP1, const sglVertex& screenP2);
	/**
	* Draws a point into the currContext
	*
	* @param screenP [in] the center of a point in screen space
	*/
	static void DrawPoint(const sglVertex& screenP);
	/**
	* Draws elements defined in a vertex buffer of a context
	* 
	* @params context [in] context for which to draw elements
	*/
	static void DrawElementsInContext(sglContext* context);

	static void printTime();
};

#endif

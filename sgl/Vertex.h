//---------------------------------------------------------------------------
// Vertex.h
// Header file Vertex in SGL (Simple Graphics Library)
// Date:  2021/10/18
// Author: Zdenek Kolar
//---------------------------------------------------------------------------
#ifndef _SGL_VERTEX_H_
#define _SGL_VERTEX_H_

/// struct for representing vertices in sgl
struct sglVertex {
    float x, y, z, w;
    sglVertex(float x = 0, float y = 0, float z = 0, float w = 0) : x(x), y(y), z(z), w(w) {};
};

#endif

//---------------------------------------------------------------------------
// context.cpp
// Implementation for contexts in SGL (Simple Graphics Library)
// Date:  2021/10/07
// Author: Zdenek Kolar
//---------------------------------------------------------------------------

#include "context.h"

sglContext::~sglContext()
{
	delete[] colorBuffer;
	delete[] ZBuffer;
	delete[] _viewport;
	delete[] _PVM;
	while (_modelViewStack.size()) {
		delete[] _modelViewStack.top();
		_modelViewStack.pop();
	}
	while (_projectionStack.size()) {
		delete[] _projectionStack.top();
		_projectionStack.pop();
	}
}

bool sglContext::init() {
	colorBuffer = new sglColor[size];
	ZBuffer = new float[size];
	_viewport = new float[16]{ 1, 0, 0, 0,
							  0, 1, 0, 0,
							  0, 0, 1, 0,
							  0, 0, 0, 1, };
	_PVM = new float[16]{ 1, 0, 0, 0,
						  0, 1, 0, 0,
						  0, 0, 1, 0,
						  0, 0, 0, 1, };

	return colorBuffer && ZBuffer && _viewport;
}

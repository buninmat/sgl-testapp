//---------------------------------------------------------------------------
// context.h
// Header file for contexts in SGL (Simple Graphics Library)
// Date:  2021/10/07
// Author: Zdenek Kolar
//---------------------------------------------------------------------------

#ifndef _SGL_CONTEXT_H_
#define _SGL_CONTEXT_H_

#include <stack>
#include <queue>
#include "sgl.h"
#include "Color.h"
#include "MatrixUtils.h"
#define MAX_STACK_SIZE 1024

/// Class representing sgl drawing context
class sglContext
{
	/// transformation stack for model-view matrices
	std::stack<float*> _modelViewStack;
	/// transformation stack for projection matrices
	std::stack<float*> _projectionStack;
	/// current stack
	std::stack<float*>* _currentStack = &_modelViewStack;
	/// viewport matrix
	float* _viewport = nullptr;
	/// projection-view-model transformation matrix
	float* _PVM = nullptr;

public:
	const int width, height, size;
	/// Color Buffer to draw to
	sglColor* colorBuffer = nullptr;
	/// Depth buffer to depth 
	float* ZBuffer = nullptr;
	/// vertex buffer
	std::queue<sglVertex> vertices;
	/// Current color for drawing
	sglColor currColor;
	/// Color to clear buffers with
	sglColor clearColor;
	/// Current point size for the context
	float pointSize = 1;
	/// Whether to use ZBuffer for depth test during drawing
	bool useZBuffer = false;
	/// what kind of elements are being stored to the _vertices 
	sglEElementType currentElementType = sglEElementType::SGL_POINTS;
	/// element filling mode
	sglEAreaMode areaMode = sglEAreaMode::SGL_FILL;

	/// canot use the default constructor
	sglContext() = delete;

	/**
	* Contructor for a drawing context
	* 
	* @param w [in] width of buffers
	* @param h [in] height of buffers
	*/
	sglContext(int w, int h) : width(w), height(h), size(w * h) {}

	/**
	* Allocated color and ZBuffer according to width and height of the context
	* 
	* @return true if the allocation was successful, false otherwise
	*/
	bool init();

	/// Calculate _PVM matrix based on current matrices in projection stack and modelviewstack
	inline void recalculatePVM() {
		sglMatrixUtils::mult(_projectionStack.top(), _modelViewStack.top(), _PVM);
	}

	/**
	* Sets new _currentStack accordingly
	* 
	* @param mode [in] which stack to make active
	*/ 
	inline void setCurrentMatrixMode(sglEMatrixMode mode) { 
		_currentStack = mode == SGL_PROJECTION ? &_projectionStack : &_modelViewStack;
	}

	/**
	* Getter for Current Matrix
	* 
	* @return Current matrix with respect to matrixMode 
	*/
	inline float* getCurrentMatrix() { return _currentStack->top(); }

	/**
	* Getter for projection-view_model matrix
	* 
	* @return current _PVM matrix
	*/
	inline float* getPVM() { return _PVM; }

	/**
	* Getter for viewport matrix
	* 
	* @return current _viewport marix
	*/
	inline float* getViewport(){ return _viewport; }

	/**
	* Checker for matrix stack overflow
	* 
	* @return whether current stack's size is bigger of equal to MAX_STACK_SIZE
	*/
	inline bool checkStackOverflow() { return (int)_currentStack->size() >= MAX_STACK_SIZE; }

	/**
	* Checker for matrix stack underflow
	* 
	* @return whether current stack's size is smaller or equal to 1
	*/
	inline bool checkStackUnderflow() { return (int)_currentStack->size() <= 1; }

	/**
	* Pushes matrix to the current stack
	* 
	* @param mat [in] matrix to be pushed
	*/
	inline void pushMatrix(float* mat) { _currentStack->push(mat); }

	/**
	* Pops matrix from the current stack
	*
	* @param mat [in] matrix to be pushed
	*/
	inline void popMatrix() { delete[] _currentStack->top(); _currentStack->pop(); }

	/// Same as functionality sglLoadMatrix
	inline void loadMatrix(const float* mat) {
		if ((int)_currentStack->size() == 0) {
			_currentStack->push(new float[16]);

		}
		for (int i = 0; i < 16; i++) { (_currentStack->top())[i] = mat[i]; }
	}
	
	/// Same functionality as sglViewport
	inline void viewport(int x, int y, int width, int height) {
		_viewport[0] = width / 2.f;
		_viewport[5] = height / 2.f;
		_viewport[12] = x + width / 2.f;
		_viewport[13] = y + height / 2.f;
	}

	/**
	* Sets pixel in color buffer to currColor
	* 
	* @param idx [in] index of pixel in colorBuffer to be set
	*/
	inline void setPixelColorAt(int idx) {
		if(idx >= 0 && idx < size)
			colorBuffer[idx] = currColor;
	}

	/**
	* Sets pixel in color buffer to currColor
	* 
	* @param x [in] x-coordinate in the image matrix
	* @param y [in] y-coordinate in the image matrix
	*/
	inline void setPixelColorAt(int x, int y){
		setPixelColorAt(x + y * width);
	}

	/// Deallocates buffers and frees matrices on stacks
	~sglContext();
};
#endif
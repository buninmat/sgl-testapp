//---------------------------------------------------------------------------
// sgl.cpp
// Empty implementation of the SGL (Simple Graphics Library)
// Date:  2016/10/24
// Author: Jaroslav Sloup
// Author: Jaroslav Krivanek, Jiri Bittner, CTU Prague
// Edited: Jakub Hendrich, Daniel Meister, CTU Prague
//---------------------------------------------------------------------------
#define _USE_MATH_DEFINES

#include <vector>
#include <limits>
#include <cmath>
#include <iostream>
#include "sgl.h"
#include "MatrixUtils.h"
#include "context.h"
#include "Renderer.h"
#include "Vertex.h"


#define MAX_CONTEXTS 32

///  Struct for representing the sgl State
struct sglState {
    /// index of the active context (-1 if none allocated)
    int currContextID = -1;
    /// current error flag
    sglEErrorCode errorCode = sglEErrorCode::SGL_NO_ERROR;
    /// is sgl in between sglBegin/sglEnd calls
    bool isRecordingVertices = false;
};

/// Array for context storage
static std::vector<sglContext*> _contexts(MAX_CONTEXTS);

/// Current state of the library
static sglState _sglState;

static inline void setErrCode(sglEErrorCode c) 
{
  if (_sglState.errorCode == SGL_NO_ERROR)
      _sglState.errorCode = c;
}

static inline bool noContextOrBeginCalled() {
    return _sglState.currContextID == -1 || _sglState.isRecordingVertices;
}

//---------------------------------------------------------------------------
// sglGetError()
//---------------------------------------------------------------------------

sglEErrorCode sglGetError(void) 
{
  sglEErrorCode ret = _sglState.errorCode;
  _sglState.errorCode = SGL_NO_ERROR;
  return ret;
}

//---------------------------------------------------------------------------
// sglGetErrorString()
//---------------------------------------------------------------------------

const char* sglGetErrorString(sglEErrorCode error)
{
  static const char *errStrigTable[] = 
  {
    "Operation succeeded",
    "Invalid argument(s) to a call",
    "Invalid enumeration argument(s) to a call",
    "Invalid call",
    "Quota of internal resources exceeded",
    "Internal library error",
    "Matrix stack overflow",
    "Matrix stack underflow",
    "Insufficient memory to finish the requested operation"
  };

  if ((int)error < (int)SGL_NO_ERROR || (int)error > (int)SGL_OUT_OF_MEMORY)
    return "Invalid value passed to sglGetErrorString()"; 

  return errStrigTable[(int)error];
}

//---------------------------------------------------------------------------
// ======================= Initialization functions =========================
//---------------------------------------------------------------------------

void sglInit(void) {}

//---------------------------------------------------------------------------
// SGL Finish (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglFinish(void) {
    for (size_t i = 0; i < _contexts.size(); i++) {
        if(_contexts[i] != nullptr)
            delete _contexts[i];
    }
}

//---------------------------------------------------------------------------
// context allocation (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

int sglCreateContext(int width, int height) {
    for (int i = 0; i < MAX_CONTEXTS; i++) {
        if (_contexts[i] == nullptr) {
            sglContext *c = new sglContext(width, height);
            if (!c->init()) {
                delete c;
                setErrCode(SGL_OUT_OF_MEMORY);
                return -1;
            }
            if (_sglState.currContextID == -1) {
                _sglState.currContextID = i;
            }
            _contexts[i] = c;
            return i;
        }
    }

    setErrCode(SGL_OUT_OF_RESOURCES);
    return -1;
}

//---------------------------------------------------------------------------
// context destruction (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglDestroyContext(int id) {
    if (id < 0 || id > MAX_CONTEXTS || _contexts[id] == nullptr) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }
    else if (id == _sglState.currContextID) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    delete _contexts[id];
    _contexts[id] = nullptr;
}

//---------------------------------------------------------------------------
// context selection (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglSetContext(int id) {
    if (id < 0 || id > MAX_CONTEXTS || _contexts[id] == nullptr) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }

    _sglState.currContextID = id;
}

//---------------------------------------------------------------------------
// context selection (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

int sglGetContext(void) {
    if (_sglState.currContextID == -1) {
        setErrCode(SGL_INVALID_OPERATION);
    }
    return _sglState.currContextID;
}


//---------------------------------------------------------------------------
// Color buffer retrieval (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

float *sglGetColorBufferPointer(void) {
    return _sglState.currContextID == -1 ? nullptr : (float*)(_contexts[_sglState.currContextID]->colorBuffer);
}

//---------------------------------------------------------------------------
// ========================== Drawing functions =============================
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Clear color (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglClearColor(float r, float g, float b, float alpha) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    _contexts[_sglState.currContextID]->clearColor.setColor(r, g, b);
}

//---------------------------------------------------------------------------
// Clear (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglClear(unsigned what) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    if(what & ~(SGL_COLOR_BUFFER_BIT | SGL_DEPTH_BUFFER_BIT)) {
        setErrCode(SGL_INVALID_VALUE);
    }

    sglContext* c = _contexts[_sglState.currContextID];
    if (what & SGL_COLOR_BUFFER_BIT) {
        for (int i = 0; i < c->size; c->colorBuffer[i++] = c->clearColor);
    }
    if (what & SGL_DEPTH_BUFFER_BIT) {
        constexpr float max = std::numeric_limits<float>::max();
        for (int i = 0; i < c->size; c->ZBuffer[i++] = max);
    }
}

//---------------------------------------------------------------------------
// Begin (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglBegin(sglEElementType mode) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    if (mode < SGL_POINTS || SGL_LAST_ELEMENT_TYPE <= mode) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }

    _contexts[_sglState.currContextID]->currentElementType = mode;
    _sglState.isRecordingVertices = true;
}

//---------------------------------------------------------------------------
// End (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglEnd(void) {
    if (!_sglState.isRecordingVertices) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    _sglState.isRecordingVertices = false;
    sglRenderer::DrawElementsInContext(_contexts[_sglState.currContextID]);
}

//---------------------------------------------------------------------------
// Vertices (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglVertex4f(float x, float y, float z, float w) {
    if (_sglState.currContextID == -1) {
        return;
    }
    _contexts[_sglState.currContextID]->vertices.push(sglVertex(x, y, z, w));
}

void sglVertex3f(float x, float y, float z) {
    sglVertex4f(x, y, z, 1);
}

void sglVertex2f(float x, float y) {
    sglVertex4f(x, y, 0, 1);
}

void sglCircle(float x, float y, float z, float radius) {
    if(radius <= 0){
        setErrCode(SGL_INVALID_VALUE);
        return;
    }
    if(noContextOrBeginCalled()){
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    if(_contexts[_sglState.currContextID]->areaMode == SGL_POINT){
        sglBegin(SGL_POINTS);
        sglVertex3f(x, y, z);
        sglEnd();
        return;
    }

    auto &context = _contexts[_sglState.currContextID];
    context->recalculatePVM();
	float* PVM = context->getPVM();
	float* viewport = context->getViewport();
    auto screenCent = sglMatrixUtils::toScreenSpace(PVM, viewport, sglVertex(x, y, z, 1));

    int xi = (int)screenCent.x;
    int yi = (int)screenCent.y;
    static float m[16];
    sglMatrixUtils::mult(viewport, PVM, m);
    radius *= sqrt(m[0] * m[5] - m[1] * m[4]);

    int dx = 0, dy = (int)radius;
    int p = (int)(1 - radius);
    int ddx = 3;
    int ddy = (int)(2 * radius - 2);
    while (dx <= dy){
        context->setPixelColorAt(xi + dx, yi + dy);
        context->setPixelColorAt(xi - dx, yi + dy);
        context->setPixelColorAt(xi + dx, yi - dy);
        context->setPixelColorAt(xi - dx, yi - dy);
        context->setPixelColorAt(xi + dy, yi + dx);
        context->setPixelColorAt(xi - dy, yi + dx);
        context->setPixelColorAt(xi + dy, yi - dx);
        context->setPixelColorAt(xi - dy, yi - dx);

        if(p > 0){
            p -= ddy;
            ddy -= 2;
            dy -= 1;
        }
        p += ddx;
        ddx += 2;
        dx += 1;
    }
    
}

void sglEllipse(float x, float y, float z, float a, float b) {
    if(a <= 0 || b <= 0){
        setErrCode(SGL_INVALID_VALUE);
        return;
    }
    if(noContextOrBeginCalled()){
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    if(_contexts[_sglState.currContextID]->areaMode == SGL_POINT){
        sglBegin(SGL_POINTS);
        sglVertex3f(x, y, z);
        sglEnd();
        return;
    }

    sglBegin(SGL_LINE_LOOP);

    static const float dang = (float)M_PI / 20;

    float c, s;
    float ang = 0;
    for(int i = 0; i < 40; ++i){
        c = std::cos(ang);
        s = std::sin(ang);
        sglVertex3f(x + c * a, y + s * b, z);
        ang += dang;
    }

    sglEnd();
}

void sglArc(float x, float y, float z, float radius, float from, float to) {
    if(radius <= 0){
        setErrCode(SGL_INVALID_VALUE);
        return;
    }
    if(noContextOrBeginCalled()){
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    static const float dang = (float)M_PI / 20;

    sglBegin(SGL_LINE_STRIP);

    to = fmodf(to, (float)M_PI * 2);
    from = fmodf(from, (float)M_PI * 2);
    float dif = to - from;
    if(dif < 0){
        dif += (float)M_PI*2;
    }

    const int n = (int)(20 * (dif / (float)M_PI));
    float c, s;
    float dx, dy;
    for(int i = 0; i < n; ++i){
        c = std::cos(from);
        s = std::sin(from);
        dx = c * radius;
        dy = s * radius;
        sglVertex3f(x + dx, y + dy, z);
        from += dang;
    }
    c = std::cos(to);
    s = std::sin(to);
    dx = c * radius;
    dy = s * radius;
    sglVertex3f(x + dx, y + dy, z);

    sglEnd();
}

//---------------------------------------------------------------------------
//========================== Transform functions ============================
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Set Matrix Mode (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglMatrixMode(sglEMatrixMode mode) {
    if (mode != SGL_MODELVIEW && mode != SGL_PROJECTION) {
        setErrCode(SGL_INVALID_ENUM);
        return;
    }
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    _contexts[_sglState.currContextID]->setCurrentMatrixMode(mode);
}

//---------------------------------------------------------------------------
// Push Matrix (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglPushMatrix(void) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    sglContext* c = _contexts[_sglState.currContextID];
    if (c->checkStackOverflow()) {
        setErrCode(SGL_STACK_OVERFLOW);
    }

    float* newMat = new float[16];
    float* currMat = c->getCurrentMatrix();
    for (int i = 0; i < 16; i++) { newMat[i] = currMat[i]; }
    c->pushMatrix(newMat);
}

//---------------------------------------------------------------------------
// Pop Matrix (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglPopMatrix(void) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    sglContext* c = _contexts[_sglState.currContextID];
    if (c->checkStackUnderflow()) {
        setErrCode(SGL_STACK_UNDERFLOW);
        return;
    }

    c->popMatrix();
}

//---------------------------------------------------------------------------
// load identity (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglLoadIdentity(void) {
    static const float IDENTITY_MATRIX[16]{ 1, 0, 0, 0,
                                            0, 1, 0, 0,
                                            0, 0, 1, 0,	
                                            0, 0, 0, 1 };
    sglLoadMatrix(IDENTITY_MATRIX);
}

//---------------------------------------------------------------------------
// Load matrix (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglLoadMatrix(const float *matrix) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    _contexts[_sglState.currContextID]->loadMatrix(matrix);
}

//---------------------------------------------------------------------------
// Load matrix (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglMultMatrix(const float *matrix) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
    }

    float* curr = _contexts[_sglState.currContextID]->getCurrentMatrix();
    sglMatrixUtils::mult(curr, matrix, curr);
}

//---------------------------------------------------------------------------
// Translate (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglTranslate(float x, float y, float z) {
    static float T[16] { 1, 0, 0, 0, 
                         0, 1, 0, 0,
                         0, 0, 1, 0,
                         x, y, z, 1 };
    T[12] = x; T[13] = y; T[14] = z;

    sglMultMatrix(T);
}

//---------------------------------------------------------------------------
// Scale (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglScale(float scalex, float scaley, float scalez) {
    static float S[16] { scalex, 0, 0, 0,
                         0, scaley, 0, 0,
                         0, 0, scalez, 0,
                         0, 0, 0, 1 };
    S[0] = scalex; S[5] = scaley; S[10] = scalez;

    sglMultMatrix(S);
}

//---------------------------------------------------------------------------
// Rotate2D (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglRotate2D(float angle, float centerx, float centery) {
    static float ROT_2D[16] { 0, 0, 0, 0,
                              0, 0, 0, 0,
                              0, 0, 1, 0,
                              0, 0, 0, 1 };
    float s = std::sin(-angle);
    float c = std::cos(-angle);
    ROT_2D[0] = c;      ROT_2D[4] = s;  ROT_2D[12] = centerx - centerx * c - centery * s;
    ROT_2D[1] = -s;     ROT_2D[5] = c;  ROT_2D[13] = centery + centerx * s - centery * c;

    sglMultMatrix(ROT_2D);
}

//---------------------------------------------------------------------------
// RotateY (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglRotateY(float angle) {
    static float R_Y[16]{ std::cos(angle), 0, std::sin(angle), 0,
                         0, 1, 0, 0,
                        -std::sin(angle), 0, std::cos(angle), 0,
                         0, 0, 0, 1 };
    R_Y[0] = cos(angle); R_Y[8] = sin(angle);
    R_Y[2] = sin(angle); R_Y[10] = cos(angle);

    sglMultMatrix(R_Y);
}

//---------------------------------------------------------------------------
// Ortho (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglOrtho(float left, float right, float bottom, float top, float near, float far) {
    static float ORTHO[16]{ 1, 0, 0, 0,
                            0, 1, 0, 0,
                            0, 0, 1, 0,
                            0, 0, 0, 1 };
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    if (left == right || bottom == top || far == near) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }

    ORTHO[0]  = 2/(right - left); ORTHO[5]  = 2/(top - bottom); ORTHO[10]  = -2/(far - near);
    ORTHO[12] = (right + left) / (left - right);
    ORTHO[13] = (top + bottom) / (bottom - top);
    ORTHO[14] = (far + near) / (near - far);

    sglMultMatrix(ORTHO);
}

//---------------------------------------------------------------------------
// Frustum (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglFrustum(float left, float right, float bottom, float top, float near, float far) {
    static float PERSP[16]{ 1, 0, 0, 0,
                            0, 1, 0, 0,
                            0, 0, 1, 0,
                            0, 0, -1,0 };
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    if (left == right || bottom == top || far == near) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }

    PERSP[0] = (2*near) / (right - left); PERSP[5] = (2*near) / (top - bottom); PERSP[10] = (far+near) / (near - far);
    PERSP[8] = (right + left) / (right - left);
    PERSP[9] = (top + bottom) / (top - bottom);
    PERSP[14] = (-2*far*near) / (far - near);

    sglMultMatrix(PERSP);
}

//---------------------------------------------------------------------------
// Viewport (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglViewport(int x, int y, int width, int height) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    if (width <= 0 || height <= 0) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }

    _contexts[_sglState.currContextID]->viewport(x, y, width, height);
}

//---------------------------------------------------------------------------
// ========================= Attribute functions ============================
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// SetColor (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglColor3f(float r, float g, float b) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    _contexts[_sglState.currContextID]->currColor.setColor(r, g, b);
}

//---------------------------------------------------------------------------
// Area mode (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglAreaMode(sglEAreaMode mode) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }

    switch (mode)
    {
    case SGL_POINT:
    case SGL_LINE:
    case SGL_FILL:
        _contexts[_sglState.currContextID]->areaMode = mode;
        break;
    default:
        setErrCode(SGL_INVALID_ENUM);
    }
}

//---------------------------------------------------------------------------
// Point size (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglPointSize(float size) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    if (size <= 0) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }

    _contexts[_sglState.currContextID]->pointSize = size;
}

//---------------------------------------------------------------------------
// Enable (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglEnable(sglEEnableFlags cap) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    if (cap & SGL_DEPTH_TEST) {
        _contexts[_sglState.currContextID]->useZBuffer = true;
        return;
    }
    setErrCode(SGL_INVALID_VALUE);
}

//---------------------------------------------------------------------------
// Disable (impl. Zdenek Kolar)
//---------------------------------------------------------------------------

void sglDisable(sglEEnableFlags cap) {
    if (noContextOrBeginCalled()) {
        setErrCode(SGL_INVALID_OPERATION);
        return;
    }
    if (cap != SGL_DEPTH_TEST) {
        setErrCode(SGL_INVALID_VALUE);
        return;
    }

    _contexts[_sglState.currContextID]->useZBuffer = false;
}

//---------------------------------------------------------------------------
// RayTracing oriented functions
//---------------------------------------------------------------------------

void sglBeginScene() {}

void sglEndScene() {}

void sglSphere(const float x,
               const float y,
               const float z,
               const float radius) 
{}

void sglMaterial(const float r,
                 const float g,
                 const float b,
                 const float kd,
                 const float ks,
                 const float shine,
                 const float T,
                 const float ior) 
{}

void sglPointLight(const float x,
                   const float y,
                   const float z,
                   const float r,
                   const float g,
                   const float b) 
{}

void sglRayTraceScene() {}

void sglRasterizeScene() {}

void sglEnvironmentMap(const int width,
                       const int height,
                       float *texels)
{}

void sglEmissiveMaterial(const float r,
                         const float g,
                         const float b,
                         const float c0,
                         const float c1,
                         const float c2)
{}